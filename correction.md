## Correction

Ici sont inscrits tous les points développés pour ce projet.
Pour chaque fonctionnalité, se trouve la Description de celle-ci ainsi que la façon de la tester

## A1
- Description:
Cette fonctionnalité consiste à l'extraction des données de la
ville de Montréal et de construire une base de données dans laquelle
vont être introduites les données extraites,  il y a suppession d'abord des données déjà introduites si elles y sont ensuite introduction de ces données à nouveau.
le script résponsable de A1 est `A1.js`.

- Test:
A1 est testée avec la ligne de commande: `node A1.js`

## A2
- Description:
Cette fonctionnalité consiste à lancer un `cron job` afin d'importer automatiquement les données importées au point `A1` chaque jour à 13h30. Le script résponsable de A2 est `A2.js`.

- Test:
A2 est testée avec la ligne de commande: `node A2.js`

## A3
- Description:
Cette fonctionnalité consiste à la création d'une application `Express.js`, aussi à la définition de la route résponsable à la génération de la documentation des services web du projet.

- Test:
L'application `Express` faite dans `A3` est lancée à l'aide de: `npm start` et la route générant la documentation est: `/doc`.

## A4
- Description: Cette fonctionnalité offre un service REST, permettant l'obtention d'une liste contenant les actes criminels pour une date donnée et specifiée dans l'url.

- Test:
cette fonctionnalité est implémentée dans le fichier `index.js` contenu dans le dossier `routes` du dossier `Express`, et comme la fonctionnalité `A3`, ça se lance à l'aide de la commande: `npm start` la route résponsable est `/actes`, suivi d'une date,

Exemple: http://localhost:3000/actes?date=2018-04-01


## A5
- Description: Cette fonctionnalité offre un service REST, permettant l'obtention de toutes les statistiques mensuelles sur les actes criminels survenus, le retour de ce service est en format `json`

- Test: Cette fonctionnalité est implémentée dans le fichier `index.js` contenu dans le dossier `routes` du dossier `Express`, tel que les fonctionnalités précédentes, ça se lance à l'aide de la commande: `npm start` la route résponsable est: http://localhost:3000/statistiques-mensuelles-json

## A6
- Description: Comme pour A5, cette fonctionnalité offre un service REST, permettant l'obtention de toutes les statistiques mensuelles sur les actes criminels survenus, le retour de ce service est en format `xml`

- Test: Cette fonctionnalité est implémentée dans le fichier `index.js` contenu dans le dossier `routes` du dossier `Express`, la route résponsable est: http://localhost:3000/statistiques-mensuelles-xml

## B1
- Description: Cette fonctionnalité offre un service permettant l'obtention de toutes les statistiques mensuelles sur les actes criminels survenus, en retournant du `HTML`. le service est invoqué par une requête Ajax.

- Test: Cette fonctionnalité est implémentée dans le fichier `index.js` contenu dans le dossier `routes` du dossier `Express`, ainsi que dans le fichier `script.js` contenu dans le dossier `public/javascripts` la route résponsable est: http://localhost:3000/statistiques-mensuelles-html

## B2
- Description: Cette fonctionnalité affiche un bar chart illustrant l’évolution des différents types d’actes criminels pour les 4 derniers mois présents dans les données. La gestion du graphique est faite à l'aide d'une librairie en `javascript`: `CanvasJS.Chart` voir les références dans le README pour plus de détails.

- Test: Cette fonctionnalité est implémentée dans le fichier `script.js` contenu dans le dossier `public/javascripts` du dossier `Express`, la route résponsable est: http://localhost:3000/statistiques-mensuelles-html

## C1
- Description: un service web retournant la liste des actes criminels ayant eu lieu les 3
derniers mois situés à moins de 3 km d’un point donné en paramètre. la distance est calculée selon la formule de haversine, le retour de ce service est en format `json`.

- Test: Cette fonctionnalité est implémentée dans le fichier `index.js` contenu dans le dossier `routes` du dossier `Express`, la route résponsable est: actes3km suivie d'une longitude et d'une latitude comme suit:
http://localhost:3000/actes-3-km?latitude=45.5372886&longitude=-73.5904142

## E1
- Description: Des services web qui gèrent des déclarations citoyennes d’événements louches.

  Une déclaration contient les éléments suivants :

  * l’identité du citoyen (nom, prénom)
  * la date et l’heure de l’observation
  * les coordonnées GPS de l’événement
  * une description textuelle de l’événement
  * un degré d’importance (un nombre de 1 à 5)
  * le nombre d’individus observés

  Les services web permettent :
   * La création d'un évennement(methode POST)
   * La modification d'un évennement déjà existant (methode PATCH avec id)
   * La suppression d'un évennement déjà existant (methode DELETE avec id)
   * La consultation d'un évennement déclaré (methode GET avec id)
   * La consultation de la liste complète des déclarations (methode GET)

- Test: Cette fonctionnalité est implémentée dans le fichier `index.js` contenu dans le dossier `routes` du dossier `Express`, elle est divisée en plusieurs(5) routes. Pour les tester il faut agir comme client (Ex: Postman) et appeler les routes suivantes:

   * Création d'un évennement:
    POST http://localhost:3000/declarations en plus d'envoyer la déclaration dans le body de la requête
   * Obtention de la liste complète des déclarations:
    GET http://localhost:3000/declarations  
   * Modification d'une déclaration selon un identifiant:
    PATCH http://localhost:3000/declarations/5b727029d5365739d8429839
   * Suppression d'une déclaration selon un identifiant:
    DELETE http://localhost:3000/declarations/5b727029d5365739d8429839
   * Obtention d'une déclaration selon un identifiant:
    GET http://localhost:3000/declarations/5b727029d5365739d8429839

   PS: pour les routes avec une méthode 'GET' on peut les appeler à partir du fureteur
