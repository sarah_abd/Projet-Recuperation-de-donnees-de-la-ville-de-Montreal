# Projet: Offrir des services à partir des données extraites du web

## Description

Dans le cadre du cours **Paradigmes des échanges Internet (INF 4375)** pour **le programme de baccalauréat en informatique et génie logiciel** de l'UQÀM

On a reçu le mandat de concevoir un projet qui sert à offrir des services web à partir de données extraites d'un site officiel de la ville de Montréal


## Auteure

- Sarah Abdelaoui (ABDS14578607)

## Plateformes supportées

Cette application a été créée et téstée sous la plateforme **macOS**


## Installation

Le framework utilisé est  **Express** pour **Node.js**, ce dernier sert à simplifier l'utilisation d'HTTP (requêtes et réponses) ainsi que le routing d'URL en fournissant une structure de projet

Pour éxécuter l'application (à partir de la fonctionnalité A3), il faut d'abord installer **Express** (les fonctionnalités A1 et A2 ne sont pas concernées par **Express**)

L'installation de **Express** se fait avec **npm** à l'aide de la commande:
```
     npm install express
```
Toutefois si l'on voudrait générer un squelette de projet **Express.js**, un logiciel pourrait être installé à l'aide de la commande:
```
     npm install -g express-generator
```

## Compilation et démarage de l'application

Pour partir le projet, il faut taper la commande `npm install` d'abord afin d'installer toutes les dépendances sur lesquelles le projet est basé, celles-ci sont contenues dans le fichier **package.json**, ensuite taper la commande `npm start` pour partir l'application.
La route principale est: `http://localhost:3000/`, celle-ci va afficher la page d'accueil.

Les routes résponsables des fonctionnalités demandées sont expliquées en détail dans le fichier correction.md

## Fonctionnalités demandées et réalisées

- A1: Permet l'extraction de données de la ville de Montréal sur les actes criminels sur le territoire de la ville et permet aussi la construction d'une base de données MongoDB avec ces données. Le script obtient les données à l’aide d’une requête HTTP

La liste des actes criminels en CSV :
http://donnees.ville.montreal.qc.ca/dataset/5829b5b0-ea6f-476f-be94-bc2b8797769a/resource/c6f482bf-bf0f-4960-8b2f-9982c211addd/download/interventionscitoyendo.csv

- A2: Permet l'extraction du code d’importation de données du point A1 dans un module réutilisable, ça permet aussi le lancement d'une cron job afin d’importer automatiquement les données chaque jour à 13h30.

- A3: Permet la création d'une application Express.js, l'implémentation de la route `/doc` résponsable de la documentation des services web du projet

- A4: Un service REST est offert permettant l'obtention d'une liste contenant les actes criminels pour une date donnée et specifiée en paramètres dans l'url, la route résponsable de cette requette est `/actes` suivi du query `date` et la valeur du query qui est la date en question en format `ISO 8601`

- A5: Un service REST est offert permettant l'obtention de toutes les statistiques mensuelles sur les actes criminels survenus, le retour de ce service est en format `json`

- A6: Permet l'obtention d'une liste de statistiques identiquement qu'en A5, le retour de ce service est en format `xml`
À completer

- B1: Permet l'obtention de toutes les statistiques mensuelles sur les actes criminels survenus, en retournant du `HTML`. le service est invoqué par une requête Ajax.

- B2: Affiche un bar chart illustrant l’évolution des différents types d’actes criminels pour les 4 derniers mois présents dans les données. La gestion du graphique est faite à l'aide d'une librairie en `javascript`: `CanvasJS.Chart` voir les références plus bas pour plus de détails.

- C1: un service web retournant la liste des actes criminels ayant eu lieu les 3
derniers mois situés à moins de 3 km d’un point donné en paramètre. la distance est calculée selon la formule de haversine

- E1: Permet des services web qui gèrent des déclarations citoyennes d’événements louches.
Les services web permettent :
 * La création d'un évennement(methode POST)
 * La modification d'un évennement déjà existant (methode PATCH avec id)
 * La suppression d'un évennement déjà existant (methode DELETE avec id)
 * La consultation d'un évennement déclaré (methode GET avec id)
 * La consultation de la liste complète des déclarations (methode GET)

## Base de données

La base de données utilisée: `MongoDB`

La création de la base de données se fait automatiquement aprés l'execution du script `A1.js`


## Technologies utilisées

- Node.js 8.11
- Express.js 4
- Pug
- MongoDB 3.6
- HTML5
- REST
- RAML
- CSS3
- Bootstrap
- Markdown
- Ajax
- JavaScript
- XML
- JSON

## Dépendances utilisées et leurs versions:
  "bootstrap": "3.3.7",
  "calendar-date-regex": "1.0.2",
  "cookie-parser": "1.4.3",
  "debug": "2.6.9",
  "express": "4.16.0",
  "express-protobuf": "1.0.1",
  "haversine": "1.1.0",
  "http-errors": "1.6.2",
  "jsonschema": "1.2.4",
  "key-value-replace": "1.0.4",
  "mongodb": "3.0.10",
  "morgan": "1.9.0",
  "nodemon": "1.18.3",
  "nunjucks": "3.1.3",
  "pug": "2.0.0-beta11",
  "querystring": "0.2.0",
  "raml2html": "3.0.1",
  "rename-keys": "2.0.1",
  "request": "2.87.0",
  "snakecase-keys-object": "1.0.1"

## Contenu du projet

À la racine du projet se trouve:
- Un dossier Express
- Un fichier database.js
- Un fichier A1.js
- Un fichier A2.js
- Un fichier README.md
- Un fichier correction.md


## Références

* https://github.com/jacquesberger/exemplesINF4375
* https://www.npmjs.com/
* https://canvasjs.com/javascript-charts/multiple-axis-column-chart/
* https://www.mongodb.com/
* https://nodejs.org/en/
