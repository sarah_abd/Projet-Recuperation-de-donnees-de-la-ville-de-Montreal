var a1 = require('./extraction-insertion.js');
var suppression = require('./suppression.js');

suppression.supprimer_collection(function(err){
  if (err) {
    console.log("Erreur de suppression");
  } else {
    // cas de succès
    console.log("Suppression des données afin d'éviter les doublons dans la bd");
  }
});
a1.lecture_csv_url(function(err){
  if (err) {
    console.log("Erreur d'insertion");
  } else {
    // cas de succès
    console.log("Les données ont bien été introduites");
  }
});
