// Module servant à la création de la base de données
function insertion_dans_bd(datas, callback) {

  const MongoClient = require("mongodb").MongoClient;
  const url         = "mongodb://localhost:27017";
  MongoClient.connect(url, function(err, client) {
    if(err){
      callback(err);
    }else{
      const db      = client.db("donnees_ville");
      console.log("Création ou mise à jour de la base de données: données_ville");
      var coll      = db.collection('interventions');
      console.log("Création ou mise à jour de la collection: interventions");
      coll.insert(datas, function (error, results) {
        console.log("Insertion ou mise à jour des données dans la collection");
        if(error){
          client.close();
          callback(error);
        }else{
          console.log("Les données ont bien été insérées ou mises à jour");
          client.close();
          callback();
        }
      });
    }
  });
}

module.exports.insertion_dans_bd = insertion_dans_bd;
