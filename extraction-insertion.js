var request           = require('request');
var URL               = "http://donnees.ville.montreal.qc.ca/dataset/5829b5b0-ea6f-476f-be94-bc2b8797769a/resource/c6f482bf-bf0f-4960-8b2f-9982c211addd/download/interventionscitoyendo.csv";
var insert            = require('./insertion.js');
var cSv               = require('csv-stream')



module.exports.lecture_csv_url = function(callback) {
  var dataset         = [];
  var csvStream       = cSv.createStream();
  csvStream._encoding = 'latin1';
  request.get({
    uri:URL
  }).pipe(csvStream)
  .on('error',function(err){
      console.error(err);
      callback(err);
  })
  .on('header', function(columns) {
  })
  .on('data',function(data){
   dataset.push(data);
  })
  .on('end',function(){
      //insertion dans la BD
     insert.insertion_dans_bd(dataset, callback);
  })
}
