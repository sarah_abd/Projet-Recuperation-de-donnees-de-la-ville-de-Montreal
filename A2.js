var CronJob     = require('cron').CronJob;
var a1          = require('./extraction-insertion.js');
var suppression = require('./suppression.js');

console.log('Désormais les données vont être mises à jour chaque jour à 13H30');
new CronJob('00 30 13 * * *', function() {
  suppression.supprimer_collection(function(err){
    if (err) {
      console.log("Erreur de suppression");
    } else {
      // cas de succès
      console.log("Suppression des données afin d'éviter les doublons dans la bd");

    }
  });
  a1.lecture_csv_url(function(err){
    if (err) {
      console.log("Erreur d'insertion");
    } else {
      // cas de succès
      console.log("Les données ont bien été introduites");
    }
  });
  console.log('Les données se sont mises à jour dans la database à 13H30');
}, null, true);
