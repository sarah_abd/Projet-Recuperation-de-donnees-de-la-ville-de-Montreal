
function text(text){
  return document.createTextNode(text);
}

function element(element){
  return document.createElement(element);
}

//fonction triant les types d'actes par ordre alphabétique
function tri(a,b){
return (a.type > b.type)?1:-1;
}

// fonctions qui retourne tous les actes possibles dans ma bd
function chargerLabels(tableau){
  var labels    = [];
  for (var i in tableau){
    for (var j in tableau[i].actes){
      labels.push(tableau[i].actes[j].type);
    }
  }
  labels        = Array.from(new Set(labels));
  return labels ;
}

// fonction construisant les donnees qui vont être utilisées par le canvas
// tableauCanvas: tableau des 4 derniers mois
// labels: tableau contenant tous les actes possibles
function construirData(tableauCanvas, labels){
  data                 = [];
  for (var label in labels){
    objet              = {};
    objet.type         = "column";
    objet.name         = labels[label];
    objet.legendText   = labels[label];
    objet.showInLegend = true;
    objet.dataPoints   = [];
    for (var i in tableauCanvas){
      item             = {};
      for (var j in tableauCanvas[i].actes){
        if (tableauCanvas[i].actes[j].type === labels[label]){
          item.label   = tableauCanvas[i].mois;
          item.y       = tableauCanvas[i].actes[j].nombre;
          objet.dataPoints.push(item);
        }
      }
    }
    data.push(objet);
  }
  return data;
}

//fonction qui bascule une serie de données
//voir source dans le README
function toggleDataSeries(e){
  if(typeof(e.dataSeries.visible) === "undefined" || e.dataSeries.visible){
    e.dataSeries.visible = false;
  }
  else{
    e.dataSeries.visible = true;
  }
  chart.render();
}

//fonction qui construit le graphique
function construireGraphique(data, nombreMois){
  var chart          = new CanvasJS.Chart("graphique", {
    animationEnabled :true,
    title: {
      text           :"Évolution des actes durant les " +
                      nombreMois + " derniers mois"
    },
    axisY: {
      title          :"Nombre d'actes",
      titleFontColor :"#4F81BC",
      lineColor      :"#4F81BC",
      labelFontColor :"#4F81BC",
      tickColor      :"#4F81BC"
    },
    toolTip: {
      shared         :true
    },
    legend: {
      cursor         :"pointer",
      itemclick      :toggleDataSeries
    },
    data             :data
  });
  chart.render();
}

function obtenirDate(dateQuatresDerniersMois){
  dateQuatresDerniersMois     = '' + dateQuatresDerniersMois;
  if (dateQuatresDerniersMois.substring(4,6) === '00' ||
      parseInt(dateQuatresDerniersMois.substring(4,6)) > 88){
      dateQuatresDerniersMois = parseInt(dateQuatresDerniersMois) - 88;
  }
  return dateQuatresDerniersMois;
}

function stockerElements(tableauCanvas, objetMois, dateQuatresDerniersMois){
  if (objetMois.mois > dateQuatresDerniersMois){
    objetMois.mois            = '' + objetMois.mois;
    objetMois.mois            = objetMois.mois.substring(0,4) +
                                '-' + objetMois.mois.substring(4,6);
    tableauCanvas.push(objetMois); // stocker les 4 derniers mois dans tableauCanvas
  }
  return tableauCanvas
}

function traitementDonnees(tableauCanvas, nombreMois, tableau, dernierJourDb){
  for (var i in tableau){
    var objetMois          = {}; // construire objet, pour le stoker dans tableauCanvas
    objetMois.mois         = tableau[i].mois;
    objetMois.mois         = parseInt(objetMois.mois.substring(0,4) +
                              objetMois.mois.substring(5,7));
    objetMois.actes        = [];
    var rangee             = element('tr'); // creer une rangee (html)
    var cell               = element('td'); // creer 3 cellules (html)
    var cell2              = element('td');
    var cell3              = element('td');
    var cellText           = text(tableau[i].mois); //creer un textnode
    cell.appendChild(cellText);
    rangee.appendChild(cell); // ajouter les éléments à la rangée
    tableau[i].actes.sort(tri);
    for (var j in tableau[i].actes){
      var item             = {};
      item.type            = tableau[i].actes[j].type;
      item.nombre          = tableau[i].actes[j].nombre;
      objetMois.actes.push(item);
      var typeActe         = text(tableau[i].actes[j].type);
      var nombreActe       = text(tableau[i].actes[j].nombre);
      cell2.appendChild(typeActe);
      cell3.appendChild(nombreActe);
      cell3.appendChild(element('br'));
      cell2.appendChild(element('br'));
      cell3.appendChild(element('hr'));
      cell2.appendChild(element('hr'));}
    rangee.appendChild(cell2);
    rangee.appendChild(cell3);
    document.getElementById("myTbody").appendChild(rangee);
    var dateQuatresDerniersMois = dernierJourDb-nombreMois;
    dateQuatresDerniersMois     = obtenirDate(dateQuatresDerniersMois);
    tableauCanvas               = stockerElements(tableauCanvas,
                                  objetMois, dateQuatresDerniersMois);}
  var labels                    = chargerLabels(tableauCanvas);
  var data                      = construirData(tableauCanvas, labels);
  construireGraphique(data, nombreMois);
}

// fonction permettant le chargement pour les x derniers mois,
// valable pour n'importe quel nombre de mois
function chargerElements(nombreMois){
  var tableauCanvas           = [];
  var xhr                     = new XMLHttpRequest();
  xhr.open("GET", "/statistiques-mensuelles-json/", true);
  xhr.onreadystatechange      = function(callback) {
    if(xhr.readyState         == 4){
      if(xhr.status           == 200){
        var tableau           = JSON.parse(xhr.responseText);
        var dernierJourDb     = parseInt(tableau[tableau.length-1].mois.substring(0,4)+
                                tableau[tableau.length-1].mois.substring(5,7));
          traitementDonnees(tableauCanvas, nombreMois, tableau, dernierJourDb);
        }else{
          alert("Error");
        }}};
  xhr.send();
}
