var express             = require('express');
var router              = express.Router();
var request             = require('request');
var db                  = require('./db.js');
var raml2html           = require('raml2html');
const snakeKeys         = require('snakecase-keys-object');
const queryString       = require('querystring');
var renameKeys          = require('rename-keys');
const calendarDateRegex = require('calendar-date-regex');
const haversine         = require('haversine');
var jsonschema          = require('jsonschema');
var monSchema           = require('./schemas');
var mongodb             = require('mongodb');


// Définition des fonctions

//fonction qui retourne True si les éléments de la route 'actes' sont valides
// False sinon
function validerRouteActes(date, tableauCles, formatDate){
  return tableauCles.length == 1 &&
         date              !== undefined &&
         calendarDateRegex({basic: false}).test(formatDate);
}

//fonction qui retourne True si les éléments de la route 'actes-3-k' sont valides
// False sinon
function validerRouteActes3k(lat, long, tableauCles){
  return tableauCles.length == 2 &&
         long              !== undefined &&
         lat               !== undefined;
}

//fonction retournant un array en javascript
//pour effectuer la fonction (aggregate) de MongoDB
function rechercherStatistiquesMensuelles(){
  return [{$group:{_id:{date:{$substr:["$DATE", 0, 7]}, acte:"$CATEGORIE"}, "total":{$sum:1},}},
          {$group:{_id:"$_id.date", actes:{$push:{type:"$_id.acte", nombre:"$total"}}}}
         ]
}

//fonction permettant de renommer les noms des clés d'un élément en particulier
function renomerLesClesId(element, nomAremplacer){
  return objetRenomme = renameKeys(element, function(key){
      if (key         === '_id'){
        return key    = nomAremplacer;
  }
});}

// fonction permettant de transformer les mois en chiffres
function transformerMoisEnChiffre(element){
  element._id         = element._id.substring(0,4) +
                       element._id.substring(5,7);
  return element._id  = parseInt(element._id);
}

// fonction permettant de transformer la date en chiffres
function transformerAnneeEnChiffre(element){
  element.DATE        = element.DATE.substring(0,4) +
                        element.DATE.substring(5,7) +
                        element.DATE.substring(8,10);
  return element.DATE = parseInt(element.DATE);
}

// Définition de la fonction qui retourne les mêmes résultats
// utilisés par les routes retournant json, xml
function obtenirStatsMensuelles(callback){
  request.get('http://localhost:3000/statistiques-mensuelles-json',
  function (err, response){
    if(err){
      callback(err);
    }else{
      var donnees = JSON.parse(response.body);
      callback(null, donnees);
    }
  });
}

function convertirEnString(element){
  element.mois = element.mois + ""; // reconvertir le mois en string
  element.mois = element.mois.substring(0,4) +
                '-' + element.mois.substring(4,6);

  return element.mois;
}

//Définition des routes


//route principale (accueil)
router.get('/', function(req, res, next){
  res.render('index',
              {title: 'Récupération de données de la ville de Montréal'}
            );
});

// route résponsable de la fonctionnalité A5
router.get('/statistiques-mensuelles-json', function(req, res){
  var resultats          = [];
  var jsonData           = {};
  var resultatRecherche  = rechercherStatistiquesMensuelles();
  var objetRenomme;
  db.getConnection(function(err, connection){
   connection.collection("interventions",function (err, coll){
     if(err){
       console.log(err)
     }else{
       coll.aggregate(resultatRecherche).toArray(function(err, results){
         if(err){
           console.log(err)
         }else{
           results.forEach(function(element){
             transformerMoisEnChiffre(element);
             element     = renomerLesClesId(element, 'mois');
             resultats.push(element);
           });
             resultats.sort(function (a, b) { // trier par ordre ascendant
               return a.mois - b.mois;
            });
          resultats.forEach(function(element){
              convertirEnString(element); // reconvertir le mois en string
            });
          res.status(200);
          res.json(resultats);
         }
       });
     }
    });
  });
});

// route résponsable de la fonctionnalité A6
router.get('/statistiques-mensuelles-xml', function(req, res){
  obtenirStatsMensuelles(function(err, data){
    var jsonData    = {};
    if(err){
      res.sendStatus(500);
    }else{
      jsonData.data = data;
      res.header("Content-Type", "application/xml");
      res.render('statistiques-xml', jsonData );
    }
  });
});

// route résponsable de la fonctionnalité B1
// voir script.js pour la suite (requête Ajax)
router.get('/statistiques-mensuelles-html', function(req, res){
  res.status(200);
  res.render('statistiques-html',
  { title: 'Statistiques mensuelles'});
});

// route résponsable de la fonctionnalité A4
// route obtenant les actes commis à une date spécifiée en paramètres
router.get('/actes', function(req, res) {
  var date        = req.query.date ;
  var objet_query = req.query;
  var resultats   = [];
  formatDate      = Object.values(objet_query)[0];
  if(!validerRouteActes(date, Object.keys(objet_query),formatDate)){
    return res.status(400).end('Saisie invalide!');
  }else{
    db.getConnection(function(err, connection){
       if (err){
        res.sendStatus(500);
      }else{
        var coll  = connection.collection("interventions");
         coll.find({DATE: date}).toArray(function(err, results){
          if (results.length == 0){
           return res.status(200)
           .end("Il n'y a pas d'actes criminels commis à cette date");
          }
          results.forEach(function(element){
            resultats.push(snakeKeys(element));
          });
          res.status(200);
          res.json(resultats);
        });
      }
    });
  }
});

// route résponsable de la fonctionnalité C1
// route permettant l'obtention des actes commis à une distance de 3 km
// durant les 3 derniers mois, pour
// une longitude et une latitude données en paramètres
router.get('/actes-3-km', function(req, res){
  var lat                       = req.query.latitude;
  var long                      = req.query.longitude;
  var objet_query               = req.query;
  var resultats                 = [];
  if(validerRouteActes3k(lat, long, Object.keys(objet_query)) === false){
    return res.status(400).end('Saisie invalide!');
  }else{
    db.getConnection(function(err, connection){
      if (err){
        res.sendStatus(500);
      }else{
        var coll                = connection.collection("interventions");
        coll.find().toArray(function(err, results){
          results.forEach(function(element){
            element.DATE        = parseInt(element.DATE.substring(0,4) + element.DATE.substring(5,7) + element.DATE.substring(8,10));
            resultats.push(snakeKeys(element));});
         var dernierJourDb      = resultats[resultats.length-1].date; // dernière date de la collection
         var troisDerniersMois  = []; // tableau dans lequel va être stocké le résultat de cette route
         const debut            = {latitude: lat, longitude: long}; // le point début de coordonnées
         resultats.forEach(function(element){
           const fin            = {latitude: element.lat, longitude: element.long}; // le point fin de coordonnées pour chaque élément dans la collection
           var nombreMois       = 2; // 0, 1 et 2 (3 derniers mois commençant par 0)
           var dateTroisDerMois = '' + (dernierJourDb - (nombreMois*100));
           if (dateTroisDerMois.substring(4,6) === '00' || parseInt(dateTroisDerMois.substring(4,6)) > 88){
             dateTroisDerMois   = parseInt(dateTroisDerMois) - 8800; } // pour que le mois tombe entre 1 et 12
           // si la date fait partie des 3 derniers mois
           // Ou que les points de début et de fin sont oui ou non compris dans le seuil de 3km
           if (element.date     >= (dateTroisDerMois) && haversine(debut, fin, {threshold: 3, unit: 'km'})){
             // reconvertir la date en string et en format date
             element.date       = '' + element.date;
             element.date       = element.date.substring(0,4) + '-' + element.date.substring(4,6) + '-' + element.date.substring(6,8);
             troisDerniersMois.push(element);}});
         res.status(200);
         res.json(troisDerniersMois);
       });}});}});

// route résponsable de la fonctionnalité E1 (création)
// route permettant la création d'un nouvel évennement louche déclaré
// insertion de la declaration dans la bd
 router.post('/declarations', function(req, res){
   var schema    = monSchema.schema_creation; // obtenir le schéma déclaré dans un module
   var donnee    = req.body;
   var resultat  = jsonschema.validate(donnee, schema); // validation de résultat reçu
   if(resultat.errors.length !== 0){
     // cas d'erreur
     console.log(resultat.errors)
     res.status(400).json(resultat);
   }else{
     // cas succès
     db.getConnection(function(err, connection){
        if(err){
         res.sendStatus(500);
       }else{
         var coll = connection.collection("evennements-louches");
         coll.insert(donnee, function(err, result){
           if(err){
             console.log(err);
             res.sendStatus(500);
           }else{
             // suivant les bonnes pratiques de REST,
             // on affiche ce qui a été crée avec un id (retourner quelque chose d'util)
             res.status(201).json(result.ops[0]);
           }});}});}});

// route résponsable de la fonctionnalité E1 (obtention)
// route permettant l'obtention de tous les évennements louches déclarés
 router.get('/declarations', function(req, res) {
   db.getConnection(function(err, db) {
     db.collection('evennements-louches', function (err, collection) {
       if(err){
         console.log(err);
         res.sendStatus(500);
       }else{
         collection.find().toArray(function(err, result) {
           if(err){
             console.log(err);
             res.sendStatus(500);
           }else{
             res.json(result);
           }});}});});});

// route résponsable de la fonctionnalité E1 (modification avec id)
 // route permettant la modification d'un évennement
 // modification d'une déclaration dans la bd
 router.patch('/declarations/:id', function(req, res, next){
   var schema          = monSchema.schema_modification;//schéma de modification
   var donnee          = req.body;
  // validation de résultat reçu
   var resultat        = jsonschema.validate(donnee, schema);
   var id              = new mongodb.ObjectId(req.params.id);
   if(resultat.errors.length !== 0){
     // cas d'erreur
     console.log(resultat.errors)
     res.status(400).json(resultat);
   }else{
     //cas succès, connection avec la base de données
     db.getConnection(function(err, connection){
        if(err){
         res.sendStatus(500);
       }else{
         var coll      = connection.collection("evennements-louches");
         coll.update({"_id": id}, donnee, function(err, result) {
           if(err){
             console.log(err);
             res.sendStatus(500);
           }else if(result.result.n === 0) {
             res.sendStatus(404).send({
                message: "Déclaration introuvable avec le id: " + req.params.id
            });;}else{
             // suivant les bonnes pratiques de REST,
             // on affiche ce qui a été modifié avec l'id
             donnee.id = req.params.id;
             res.status(200).json(donnee);
           }});}});}});

// route résponsable de la fonctionnalité E1 (obtention avec id)
// route permettant l'otention d'une déclaration avec un id
router.get('/declarations/:id', function(req, res){
  var id = req.params.id
  db.getConnection(function(err, db){
    db.collection('evennements-louches', function (err, collection) {
      if(err){
        console.log(err);
        res.sendStatus(500);
      }else{
        collection.findOne({"_id": new mongodb.ObjectId(id)},
         function(err, result) {
          if(err){
            console.log(err);
            res.sendStatus(500);
          }else{
            res.json(result);
          }});}});});});

// route résponsable de la fonctionnalité E1 (suppression)
// route permettant la suppression d'une déclaration
router.delete('/declarations/:id', function(req, res){
  // obtenir connection à la base de données
  db.getConnection(function(err, connection){
    if(err){
      console.log(err);
      res.sendStatus(500);
    }else{
      var coll = connection.collection("evennements-louches");
      coll.remove({_id: new mongodb.ObjectId(req.params.id)}, function(err, result){
        if(err){
          console.log(err);
          res.sendStatus(500);
        }else if(result.result.n === 0){
          res.sendStatus(404).send({
             message: "Suppression impossible! déclaration introuvable avec le id: " +
            req.params.id
         });}else{
          // retourner le code de statut, sans pour autant
          // afficher quelque chose d'util,
          // selon les principes de REST; ce n'est pas recommandé.
          res.sendStatus(200);
        }});}});});

// route résponsable de la fonctionnalité A3
// route fournissant la documentation des services REST retournant XML ou JSON
// le template générant la doc est un fichier RAML,
// situé dans le dossier: routes/doc
router.get('/doc', function(req, res) {
  var configuration = raml2html.getDefaultConfig(false);
  var onError       = function (err) {
    console.log(err);
    res.sendStatus(500);
  };
  var onSuccess     = function(html) {
    res.send(html);
  };
  raml2html.render("routes/doc/index.raml", configuration).then(onSuccess, onError);
});

module.exports      = router;
