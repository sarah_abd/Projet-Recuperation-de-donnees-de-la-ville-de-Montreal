
var MongoClient       = require("mongodb").MongoClient;
var url               = "mongodb://localhost:27017"
var instanceMongoDB;

module.exports
.getConnection        = function(callback) {
  if (instanceMongoDB) {
    callback(null, instanceMongoDB);
  } else {
    MongoClient.connect(url, function(err, client) {
      var db          = client.db("donnees_ville");
      instanceMongoDB = db;
      callback(null, db);
    });
  }
};
