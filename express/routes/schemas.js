var schema_creation = {
 "type": "object",
 "required": true,
 "properties": {
   "nom": {
     "type": "string",
     "required": true
   },
   "prenom": {
     "type": "string",
     "required": true
   },
   "date": {
     "type": "string",
     "pattern": "^\\d{4}\\-(0?[1-9]|1[012])\\-(0?[1-9]|[12][0-9]|3[01])$",
     "required": true
   },
   "heure": {
     "type": "string",
     "pattern": "^([0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$",
     "required": true
   },
   "latitude": {
     "type": "number",
     "minimum": -90,
     "maximum": 90,
     "required": true
   },
   "longitude": {
     "type": "number",
     "minimum": -180,
     "maximum": 180,
     "required": true
   },
   "description": {
     "type": "string",
     "required": true
   },
   "degre-importance": {
     "type": "number",
     "minimum": 1,
     "maximum": 5,
     "required": true
   },
   "nombre-individus": {
     "type": "number",
     "minimum": 1,
     "required": true
   }
 },
 "additionalProperties": false
}

var schema_modification = {
 "type": "object",
 "required": true,
 "properties": {
   "nom": {
     "type": "string",
     "required": false
   },
   "prenom": {
     "type": "string",
     "required": false
   },
   "date": {
     "type": "string",
     "pattern": "^\\d{4}\\-(0?[1-9]|1[012])\\-(0?[1-9]|[12][0-9]|3[01])$",
     "required": false
   },
   "heure": {
     "type": "string",
     "pattern": "^([0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$",
     "required": false
   },
   "latitude": {
     "type": "number",
     "minimum": -90,
     "maximum": 90,
     "required": false
   },
   "longitude": {
     "type": "number",
     "minimum": -180,
     "maximum": 180,
     "required": false
   },
   "description": {
     "type": "string",
     "required": false
   },
   "degre-importance": {
     "type": "number",
     "minimum": 1,
     "maximum": 5,
     "required": false
   },
   "nombre-individus": {
     "type": "number",
     "minimum": 1,
     "required": false
   }
 },
 "additionalProperties": false
}


module.exports.schema_creation = schema_creation;
module.exports.schema_modification = schema_modification;
