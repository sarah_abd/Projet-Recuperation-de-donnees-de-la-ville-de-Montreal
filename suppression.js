function supprimer_collection(callback){

  const MongoClient = require("mongodb").MongoClient;
  const url         = "mongodb://localhost:27017";
  MongoClient.connect(url, function(err, client) {
    if(err){
      callback(err);
    }else{
      const db      = client.db("donnees_ville");
      var coll      = db.collection('interventions');
      coll.drop(function (error, delOK){
        if (error){
          client.close();
          callback(error);
        }else{
          client.close();
          callback();
        }
      });
    }
  });
}

module.exports.supprimer_collection = supprimer_collection;
